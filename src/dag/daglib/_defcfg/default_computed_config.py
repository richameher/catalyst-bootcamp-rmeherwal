#!/usr/bin/env python

import os
import boto3
from urllib.parse import urlparse
from daglib.config import computed_value, ConfigItemNotFoundError, S3Url

@computed_value('project.secrets.githubssh')
def _github_secret_id(cfgobj):
    return "{environment.name}/mlproject/{project.name}/githubssh"

# Spawn gives us the http url to the git repo.  We need to compute
# the "normal" (ssh) one that we need for our project
@computed_value('project.git_url')
def _project_git_url(cfgobj):
    url = urlparse(cfgobj._root.project.git_url_http)
    return f"git@{url.netloc}:{url.path[1:]}"

@computed_value('project.default_git_branch')
def _project_default_git_branch(cfgobj):
    env = cfgobj._root.environment.name
    return cfgobj._root.project.environment_to_git_branch[env]

@computed_value('environment.name')
def _env_name(cfgobj):
    return os.environ['ENVIRONMENT']

@computed_value('environment.account_alias')
def _acct_alias(cfgobj):
    iam = boto3.client('iam')
    aliases = iam.list_account_aliases().get('AccountAliases', [])
    if aliases:
        return aliases[0]
    else:
        raise ConfigItemNotFoundError("environment.account_alias")

@computed_value('environment.region')
def _aws_region(cfgobj):
    session = boto3.session.Session()
    return session.region_name

@computed_value('environment.account_id')
def _aws_account_id(cfgobj):
    sts = boto3.client('sts')
    id = sts.get_caller_identity()
    return id['Account']

@computed_value('project.bucket')
def _project_bucket(cfgobj):
    return (
        "{environment.name}-{environment.account_alias}-"
        "{environment.region}-mlp-{project.name}"
    )

@computed_value('project.batch.job_queue_arn')
def _project_batch_job_queue_arn(cfgobj):
    return (
        "arn:aws:batch:{environment.region}:{environment.account_id}:"
        "job-queue/{environment.name}-{project.name}-JobQueue"
    )

@computed_value('project.batch.job_definition_arn')
def _project_batch_job_definition_arn(cfgobj):
    return (
        "arn:aws:batch:{environment.region}:{environment.account_id}:"
        "job-definition/{environment.name}-ml-{project.name}-jobdef"
    )

@computed_value('project.batch.role_arn')
def _project_sagemaker_role_arn(cfgobj):
    return (
        "arn:aws:iam::{environment.account_id}:role/"
        "{environment.name}-{project.name}-batch-service-role"
    )

@computed_value('project.sagemaker.role_arn')
def _project_sagemaker_role_arn(cfgobj):
    return (
        "arn:aws:iam::{environment.account_id}:role/"
        "{environment.name}-{project.name}-sagemaker-role"
    )

@computed_value('infoarch.root')
def _infoarch_root(cfgobj):
    return "s3://{project.bucket}"

# Infoarch RAW zone
def _infoarch_raw(cfgobj):
    return S3Url("s3://{project.bucket}/raw")
@computed_value('infoarch.raw.landing')
def _infoarch_raw_landing(cfgobj):
    return _infoarch_raw(cfgobj).join('landing').url
@computed_value('infoarch.raw.bad')
def _infoarch_raw_bad(cfgobj):
    return _infoarch_raw(cfgobj).join('bad').url
@computed_value('infoarch.raw.clean')
def _infoarch_raw_clean(cfgobj):
    return _infoarch_raw(cfgobj).join('clean').url
@computed_value('infoarch.raw.state')
def _infoarch_raw_status(cfgobj):
    return _infoarch_raw(cfgobj).join('meta/state').url

# Infoarch STAGING zone
def _infoarch_staging(cfgobj):
    return S3Url("s3://{project.bucket}/staging")
@computed_value('infoarch.staging.landing')
def _infoarch_staging_landing(cfgobj):
    return _infoarch_staging(cfgobj).join('landing').url
@computed_value('infoarch.staging.clean')
def _infoarch_staging_landing(cfgobj):
    return _infoarch_staging(cfgobj).join('clean').url
@computed_value('infoarch.staging.tmp')
def _infoarch_staging_landing(cfgobj):
    return _infoarch_staging(cfgobj).join('tmp').url
@computed_value('infoarch.staging.bad')
def _infoarch_staging_landing(cfgobj):
    return _infoarch_staging(cfgobj).join('bad').url

# Infoarch LABELED zone
def _infoarch_labeled(cfgobj):
    return S3Url("s3://{project.bucket}/labeled")
@computed_value('infoarch.labeled.landing')
def _infoarch_labeled_landing(cfgobj):
    return _infoarch_labeled(cfgobj).join('landing').url
@computed_value('infoarch.labeled.clean')
def _infoarch_labeled_clean(cfgobj):
    return _infoarch_labeled(cfgobj).join('clean').url
@computed_value('infoarch.labeled.bad')
def _infoarch_labeled_bad(cfgobj):
    return _infoarch_labeled(cfgobj).join('bad').url
@computed_value('infoarch.labeled.state')
def _infoarch_labeled_state(cfgobj):
    return _infoarch_labeled(cfgobj).join('meta/state').url

# Infoarch TRAINING zone
def _infoarch_training(cfgobj):
    return S3Url("s3://{project.bucket}/model/{project.model_name}")
@computed_value('infoarch.model.meta.jobspec')
def _infoarch_meta_jobspec(cfgobj):
    return _infoarch_training(cfgobj).join('meta/jobspec').url
@computed_value('infoarch.model.meta.deploy')
def _infoarch_training_deploy(cfgobj):
    return _infoarch_training(cfgobj).join('meta/deploy').url
@computed_value('infoarch.model.training')
def _infoarch_training_jobs(cfgobj):
    return _infoarch_training(cfgobj).join('training').url

import os
import sys
from time import time, sleep
import subprocess
import shlex
import tempfile
import json

from airflow.models.baseoperator import BaseOperator
from airflow.models.taskinstance import TaskInstance
from airflow.utils.state import State
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.base_sensor_operator import BaseSensorOperator
from airflow.utils.decorators import apply_defaults

import boto3

from daglib.config import MLProjectConfig

def build_batch_tmp_code_url(id=None):
    c = MLProjectConfig()
    if id:
        baseurl = c.infoarch.root.join(f"code/tmp/{id}/{time():.3f}")
    else:
        baseurl = c.infoarch.root.join(f"code/tmp/default/{time():.3f}")
    return baseurl

def push_codebase_to_s3(sourcedir, desturl):
    savedir = os.getcwd()
    os.chdir(sourcedir)

    githash = None
    if os.path.exists('.git'):
        git_cmd = shlex.split("git rev-parse HEAD")
        githash = subprocess.check_output(git_cmd)

    cp_cmd = shlex.split(f"aws s3 cp --exclude .git/* --recursive . {desturl.urlslash}")
    subprocess.check_output(cp_cmd)

    if not githash:
        githash = "not_a_git_repo".encode('utf-8')

    with tempfile.NamedTemporaryFile() as tf:
        tf.write(githash)
        tf.flush()
        cp_cmd = shlex.split(f"aws s3 cp {tf.name} {desturl.join('.git_hash')} --content-type text/plain;charset=UTF-8")
        subprocess.check_output(cp_cmd)

    os.chdir(savedir)

def _submit_core_python_batch(codeurl,
                                entrypoint,
                                queue,
                                job_defn,
                                conda_reqs_file=None,
                                vcpus=None,
                                memory=None,
                                batch_timeout=None,
                                extra_pythonpath=None,
                                func_args=None,
                                func_kwargs=None):
    batch_args = []
    batch_args.append(entrypoint)
    batch_args.extend(shlex.split(f"--codeurl {codeurl.url}"))
    batch_args.append("--py-args")
    batch_args.append(json.dumps(func_args or []))
    batch_args.append("--py-kwargs")
    batch_args.append(json.dumps(func_kwargs or {}))

    if conda_reqs_file:
        batch_args.extend(shlex.split(f"--py-reqsfile {conda_reqs_file}"))

    pythonpath = []
    if extra_pythonpath:
        pythonpath.extend(extra_pythonpath)
    pythonpath.append('src/batch')

    batch_args.append("--pythonpath")
    batch_args.extend(pythonpath)

    job_params = {}
    job_params['jobName'] = 'auto_test_1'
    job_params['jobQueue'] = queue
    job_params['jobDefinition'] = job_defn
    if batch_timeout:
        job_params['timeout']['attemptDurationSeconds'] = batch_timeout

    container_overrides = {}
    container_overrides['command'] = batch_args
    if vcpus:
        container_overrides['vcpus'] = vcpus
    if memory:
        container_overrides['memory'] = memory
    job_params['containerOverrides'] = container_overrides

    batch = boto3.client('batch')
    return batch.submit_job(**job_params)

def common_job_setup(sourcedir):
    """
    Return: codeurl, queue job_defn in a dict we can pass
    to the core submission function
    """
    c = MLProjectConfig()
    codeurl = build_batch_tmp_code_url()
    push_codebase_to_s3(sourcedir, codeurl)
    queue = c.project.batch.job_queue_arn
    job_defn = c.project.batch.job_definition_arn
    return {
        'codeurl': codeurl,
        'queue': queue,
        'job_defn': job_defn
    }

def submit_python_job(script_file,
                        sourcedir,
                        function=None,
                        conda_reqs_file=None,
                        vcpus=None,
                        memory=None,
                        batch_timeout=None,
                        func_args=None,
                        func_kwargs=None):
    core_args = common_job_setup(sourcedir=sourcedir)

    pythonpath=None
    if '/' in script_file:
        if script_file.startswith('/'):
            raise ValueError("script_file path must be relative")
        pythonpath = os.path.dirname(script_file)
        script_file = os.path.basename(script_file)
    module_name = script_file.replace('.py', '')
    entrypoint = f"{module_name}:{function or 'run'}"

    batch_ret = _submit_core_python_batch(
        **core_args,
        conda_reqs_file=conda_reqs_file,
        entrypoint=entrypoint,
        vcpus=vcpus,
        memory=memory,
        batch_timeout=batch_timeout,
        func_args=func_args,
        func_kwargs=func_kwargs,
        extra_pythonpath=pythonpath
    )
    return batch_ret

def submit_r_job(script_file,
                        sourcedir,
                        function=None,
                        conda_reqs_file=None,
                        vcpus=None,
                        memory=None,
                        batch_timeout=None,
                        func_args=None,
                        func_kwargs=None):
    core_args = common_job_setup(sourcedir)

    pythonpath = ['/exec-shim']
    entrypoint = 'shims:run_r'

    if func_kwargs is None:
        func_kwargs = {}
    func_kwargs['shim_user_file'] = script_file
    func_kwargs['shim_user_func'] = function or 'run'

    batch_ret = _submit_core_python_batch(
        **core_args,
        conda_reqs_file=conda_reqs_file,
        entrypoint=entrypoint,
        vcpus=vcpus,
        memory=memory,
        batch_timeout=batch_timeout,
        func_args=func_args,
        func_kwargs=func_kwargs,
        extra_pythonpath=pythonpath
    )
    return batch_ret


def submit_shell_job(script_file,
                        sourcedir,
                        function=None,
                        conda_reqs_file=None,
                        vcpus=None,
                        memory=None,
                        batch_timeout=None,
                        func_args=None,
                        func_kwargs=None):
    core_args = common_job_setup(sourcedir)

    pythonpath = ['/exec-shim']
    entrypoint = 'shims:run_shell'

    if func_kwargs is None:
        func_kwargs = {}
    func_kwargs['shim_user_file'] = script_file

    batch_ret = _submit_core_python_batch(
        **core_args,
        conda_reqs_file=conda_reqs_file,
        entrypoint=entrypoint,
        vcpus=vcpus,
        memory=memory,
        batch_timeout=batch_timeout,
        func_args=func_args,
        func_kwargs=func_kwargs,
        extra_pythonpath=pythonpath
    )
    return batch_ret

def find_file_type(filename):
    if filename.lower().endswith('.py'):
        return 'python'
    if filename.lower().endswith('.r'):
        return 'r'
    return 'shell'

def find_job_submitter_for_file_type(filename):
    filetype = find_file_type(filename)
    if filetype == 'python':
        return submit_python_job
    if filetype == 'r':
        return submit_r_job
    return submit_shell_job

class AWSBatchJobStatusWatcher(object):
    """
    """
    def __init__(self, op):
        self._op = op
        self._ref_status = 'SUBMITTED'
        self._job = None
        self._reused = False

    @property
    def done(self):
        self.update_status()
        if self._job['status'] in ('SUCCEEDED', 'FAILED'):
            return True
        return False

    @property
    def success(self):
        self.update_status()
        if self._job['status'] == 'SUCCEEDED':
            return True
        return False

    @property
    def failed(self):
        self.update_status()
        if self._job['status'] == 'FAILED':
            return True
        return False

    @property
    def status(self):
        self.update_status()
        return self._job['status']

    @property
    def batch_job_id(self):
        return self._op.batch_job_id

    @property
    def status_reason(self):
        self.update_status()
        return self._job['statusReason']

    def update_status(self):
        batch = boto3.client('batch')
        self._job = batch.describe_jobs(jobs=[self.batch_job_id])['jobs'][0]
        if not self._reused:
            self._reused = True
            self._op.log.info(f"Job {self.batch_job_id} status : "
                                f"{self._job['status']}")
        else:
            if self._ref_status != self._job['status']:
                self._op.log.info(f"Job {self.batch_job_id} status change: "
                                    f"{self._ref_status or 'SUBMITTED'} -> "
                                    f"{self._job['status']}")
                self._ref_status = self._job['status']

    def kill(self):
        batch = boto3.client('batch')
        status = self.status
        if self.done:
            self._op.log.warn(f"Attempted to kill job {self.batch_job_id} which "
                                f"is already in status {self.status}")
            return
        if status in ('STARTING', 'RUNNING'):
            self._op.log.info(f"Terminating job {self.batch_job_id} from status {self.status}")
            batch.terminate_job(
                jobId=self.batch_job_id,
                reason="The controlling Airflow task has been killed."
            )
        else:
            self._op.log.info(f"Cancelling job {self.batch_job_id} from status {self.status}")
            batch.cancel_job(
                jobId=self.batch_job_id,
                reason="The controlling Airflow task has been killed."
            )

    def wait(self, interval=60):
        self._op.log.info(f"Waiting for job ID {self.batch_job_id} to finish...")
        while not self.done:
            sleep(interval)
        self._op.log.info(f"Final status of job {self.batch_job_id}: {self.status}: "
                            f"{self.status_reason}")
        return self._job

class phDataAWSBatchOperator(BaseOperator):
    ui_color = '#33ccff'
    ui_fgcolor = '#000000'
    @apply_defaults
    def __init__(
            self,
            script_file,
            sourcedir=None,
            function=None,
            conda_reqs_file=None,
            vcpus=None,
            memory=None,
            batch_timeout=None,
            func_args=None,
            func_kwargs=None,
            wait=True,
            *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.script_file = script_file
        self.function = function
        self.conda_reqs_file=conda_reqs_file
        self.sourcedir=sourcedir
        self.vcpus = vcpus
        self.memory = memory
        self.batch_timeout = batch_timeout
        self.func_args = func_args
        self.func_kwargs = func_kwargs
        self.wait = wait

    def execute(self, context):
        if self.sourcedir is None:
            task_instance = context['task_instance']
            dag = context['dag']
            if dag.is_subdag:
                dag = dag.parent_dag
            sourcedir = task_instance.xcom_pull(key="project_code_directory",
                                                dag_id=dag.dag_id)
        else:
            sourcedir = self.sourcedir

        submitter = find_job_submitter_for_file_type(self.script_file)

        job = submitter(
            script_file=self.script_file,
            conda_reqs_file=self.conda_reqs_file,
            sourcedir=sourcedir,
            function=self.function,
            vcpus=self.vcpus,
            memory=self.memory,
            batch_timeout=self.batch_timeout,
            func_args=self.func_args,
            func_kwargs=self.func_kwargs
        )
        self.batch_job_id = job['jobId']
        self.log.info(f"Created job: {self.batch_job_id}.")
        if not self.wait:
            self.log.info("NOT waiting for job to finish.")
            return job['jobId']

        status_watcher = AWSBatchJobStatusWatcher(self)
        final_status = status_watcher.wait()
        if status_watcher.success:
            return final_status['jobId']
        raise Exception(f"Job {self.batch_job_id} FAILED: {status_watcher.status_reason}")

    def on_kill(self):
        status_watcher = AWSBatchJobStatusWatcher(self)
        status_watcher.kill()
        super().on_kill()

class phDataAWSBatchKillOperator(BaseOperator):
    template_fields = ['batch_job_id']
    ui_color = '#ffb3b3'
    ui_fgcolor = '#000000'
    @apply_defaults
    def __init__(self,
                    batch_job_id,
                    wait=True,
                    *args, **kwargs
                ):
        self.batch_job_id = batch_job_id
        self.wait = wait
        super().__init__(*args, **kwargs)

    def execute(self, context):
        status_watcher = AWSBatchJobStatusWatcher(self)
        status_watcher.kill()
        if self.wait:
            status_watcher.wait()

class phDataAWSBatchSensor(BaseSensorOperator):
    template_fields = ['batch_job_id']
    ui_color = '#ffcc00'
    ui_fgcolor = '#000000'
    @apply_defaults
    def __init__(self,
                    batch_job_id,
                    *args, **kwargs
                ):
        self.batch_job_id = batch_job_id
        super().__init__(*args, **kwargs)

    def poke(self, context):
        status_watcher = AWSBatchJobStatusWatcher(self)
        if status_watcher.done:
            if not status_watcher.success:
                raise Exception(f"Job {self.batch_job_id} FAILED: "
                                f"{status_watcher.status_reason}")
            else:
                return True
        return False

    def on_kill(self):
        status_watcher = AWSBatchJobStatusWatcher(self)
        status_watcher.kill()
        super().on_kill()

class phDataStatusReflectingDummyOperator(BaseOperator):
    template_fields = ['reflected_task_id']
    ui_color = '#ccff66'
    ui_fgcolor = '#000000'
    @apply_defaults
    def __init__(self,
                    reflected_task_id,
                    wait=True,
                    *args, **kwargs
                ):
        self.reflected_task_id = reflected_task_id
        super().__init__(*args, **kwargs)

    def execute(self, context):
        reflected_task = context['dag'].get_task(self.reflected_task_id)
        reflected_taskinstance = TaskInstance(reflected_task,
                                                context['execution_date'])
        reflected_state = reflected_taskinstance.current_state()
        self.log.info(f"State of reflected task {self.reflected_task_id} "
                        f"is {reflected_state}.")
        if reflected_state == State.FAILED:
            raise Exception(f"Reflected task {self.reflected_task_id} did "
                            "not succeed.  Failing.")

def aws_batch_sensor_helper(batch_operator):
    batch_tid = batch_operator.task_id
    sensor_id = f"wait_{batch_tid}"
    killer_id = f"kill_{batch_tid}"
    join_id = f"join_{batch_tid}"

    if batch_operator.wait:
        raise ValueError("Your phDataAWSBatchOperator must have wait=False "
                            "to use the aws_batch_sensor_helper.")

    sensor = phDataAWSBatchSensor(
        dag=batch_operator.dag,
        task_id=sensor_id,
        mode="reschedule",

        batch_job_id=f"{{{{ task_instance.xcom_pull('{batch_tid}') }}}}"

    )
    killer = phDataAWSBatchKillOperator(
        dag=batch_operator.dag,
        task_id=killer_id,
        trigger_rule='all_failed',

        batch_job_id=f"{{{{ task_instance.xcom_pull('{batch_tid}') }}}}"

    )
    join = phDataStatusReflectingDummyOperator(
        dag=batch_operator.dag,
        task_id=join_id,
        trigger_rule='all_done',
        reflected_task_id=sensor_id
    )

    batch_operator >> sensor >> killer >> join
    sensor >> join

    return join

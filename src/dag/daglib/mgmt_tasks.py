import os
import sys
from datetime import datetime
import subprocess
import tempfile
import shutil
from collections import namedtuple

from airflow.operators.python_operator import PythonOperator

from daglib.config import MLProjectConfig

SHARED_TMP_LOCATION = "/var/lib/airflow/tmp"

args = {"owner": "Airflow", "start_date": datetime(2019, 11, 12)}


def setup_task(**kwargs):
    c = MLProjectConfig()
    if not os.path.exists(SHARED_TMP_LOCATION):
        os.makedirs(SHARED_TMP_LOCATION)
    task_instance = kwargs["task_instance"]
    temp_directory = tempfile.mkdtemp(dir=SHARED_TMP_LOCATION)
    print(f"setup: Created temp directory '{temp_directory}")
    task_instance.xcom_push(key="temp_directory", value=temp_directory)

    print(f"setup: Clone git repo '{c.project.git_url}")
    os.chdir(temp_directory)
    git_clone_command = (
        f"git clone "
        f"--branch {c.project.default_git_branch} "
        f"{c.project.git_url} project-code"
    )
    print(f"setup: git_clone_command: {git_clone_command}")
    subprocess.check_call(git_clone_command, shell=True)
    task_instance.xcom_push(
        key="project_code_directory", value=os.path.join(temp_directory, "project-code")
    )

    reqs = c.airflow.python_requirements
    if reqs:
        temp_installs_dir = tempfile.mkdtemp(dir=temp_directory)
        print(f"setup: Install python packages {reqs} to {temp_installs_dir}")
        os.environ["PYTHONUSERBASE"] = temp_installs_dir
        subprocess.call(f"pip install --user {' '.join(reqs)}", shell=True)
        temp_installs_path = os.path.join(
            temp_installs_dir,
            f"lib/python{sys.version_info.major}.{sys.version_info.minor}/site-packages",
        )
        task_instance.xcom_push(key="temp_installs_path", value=temp_installs_path)


def teardown_task(**kwargs):
    task_instance = kwargs["task_instance"]
    temp_directory = task_instance.xcom_pull(key="temp_directory")

    print(f"teardown: removing temp directory '{temp_directory}")
    shutil.rmtree(temp_directory, ignore_errors=True)
    print(f"teardown: Done removing temp directory '{temp_directory}''")


def create_mgmt_tasks(dag):
    MgmtTasks = namedtuple("MgmtTasks", ["setup", "teardown"])
    setup = PythonOperator(
        task_id="setup", python_callable=setup_task, provide_context=True, dag=dag
    )

    teardown = PythonOperator(
        task_id="teardown", python_callable=teardown_task, provide_context=True, dag=dag
    )

    return MgmtTasks(setup=setup, teardown=teardown)

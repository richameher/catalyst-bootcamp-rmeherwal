#!/usr/bin/env python

from weakref import proxy
import os
import sys
import importlib
import yaml
import boto3
from yaml import SafeLoader
from urllib.parse import urlparse

mydir = os.path.dirname(__file__)
DEFAULT_CONFIG_FILE_PATH=os.path.realpath(os.path.join(mydir, "../config.yaml"))

class ConfigItemNotFoundError(Exception): pass
class ConfigExtNotFoundError(Exception): pass

COMPUTED_VALUE_MAP = {}
def computed_value(path):
    def decorator(func):
        COMPUTED_VALUE_MAP[path] = func
        return func
    return decorator

class S3Url(object):
    def __init__(self, url):
        self._parsed_url = urlparse(url)
        if self._parsed_url.scheme != 's3':
            raise ValueError(f"URL {url} is not a S3 URL!")

    @property
    def bucket(self):
        return self._parsed_url.netloc

    @property
    def path(self):
        retpath = self._parsed_url.path[1:]
        while retpath.endswith("/"):
            retpath = retpath[:-1]
        return retpath

    @property
    def pathslash(self):
        return self.path + "/"

    @property
    def url(self):
        return f"s3://{self.bucket}/{self.path}"

    @property
    def urlslash(self):
        return f"s3://{self.bucket}/{self.path}/"

    def join(self, other):
        joinpath = other
        # Strip leading /.  This is a bit different
        # than os.path.join normal behavior but should
        # make it easier for our use case
        while joinpath.startswith("/"):
            joinpath = joinpath[1:]
        return S3Url(os.path.join(self.url, joinpath))

    def __str__(self):
        return self.url

class MLProjectConfigView(object):
    def __init__(self, root, stack=[]):
        self._root = root
        self._stack = stack

    def __resolve_stack_config(self):
        curr_value = self._root._config_data
        for attr in self._stack:
            curr_value = curr_value[attr]
        return curr_value

    def __is_possible_attr(self, requested_key):
        for key in COMPUTED_VALUE_MAP.keys():
            if requested_key == key:
                return True
            if key.startswith(requested_key + '.'):
                return True
        return False

    def __getattr__(self, attr):
        try:
            curr_dict = self.__resolve_stack_config()
            val = curr_dict[attr]
        except KeyError:
            computed_value_key = '.'.join(self._stack + [attr])
            if computed_value_key in COMPUTED_VALUE_MAP:
                val = COMPUTED_VALUE_MAP[computed_value_key](self)
            else:
                if self.__is_possible_attr(computed_value_key):
                    return MLProjectConfigView(self._root, self._stack + [attr])
                else:
                    raise ConfigItemNotFoundError('.'.join(self._stack + [attr]))

        if type(val) is dict:
            return MLProjectConfigView(self._root, self._stack + [attr])
        elif type(val) is str:
            if '{' in val:
                val = val.format_map(self._root)
            if val.startswith('s3://'):
                return S3Url(val)
            else:
                return val
        else:
            return val
    __getitem__ = __getattr__

class MLProjectConfig(MLProjectConfigView):
    def __init__(self, config_file=DEFAULT_CONFIG_FILE_PATH):
        super().__init__(proxy(self))
        self._config_file_path = config_file
        self._config_data = {}
        self._active_jobspec = {}

        configf = open(self._config_file_path)
        self._config_data = yaml.load(configf, Loader=SafeLoader)

        importlib.import_module('daglib._defcfg.default_computed_config')

        if 'meta' in self._config_data:
            config_ext_modules = self._config_data['meta'].get('config_ext_modules', [])
            for mod in config_ext_modules:
                try:
                    importlib.import_module(mod)
                except ModuleNotFoundError:
                    raise ConfigExtNotFoundError(f"{mod} is not a valid config extension module.")

    def lookup(self, name):
        parts = name.split('.')
        value = self
        for part in parts:
            value = getattr(value, part)
        return value
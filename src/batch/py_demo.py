"""
Minimal script for demoing the AWSBatchOperator included in this repo.
"""
import logging
import os

logger = logging.getLogger(__name__)


# TODO: Update the prototype of main to accept the command line args
# you pass in your batch job
def run(*args, **kwargs) -> int:
    """
    Main function for performing batch processing of your ingested data.

    Parameters
    ----------
    TODO: You should tailor your argument list to match the func_args
    and func_kwargs you pass in your DAG.

    Returns
    -------
    None

    """
    print(f"Project environment: {os.environ['ENVIRONMENT']}")
    print(f"Args: {args} KWArgs: {kwargs}!")

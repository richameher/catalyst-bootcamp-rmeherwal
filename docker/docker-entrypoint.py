#!/opt/conda/bin/python

import sys
import os
import argparse
import subprocess
import logging
import json
import tempfile
import importlib

from collections import namedtuple

import boto3

logging.basicConfig(format="[%(asctime)-15s] - %(message)s")
LOG = logging.getLogger(__file__)
LOG.setLevel(logging.INFO)

def json_list(list_string):
    list_json = json.loads(list_string)
    if isinstance(list_json, list):
        return list_json
    else:
        raise ValueError()

def json_dict(dict_string):
    dict_json = json.loads(dict_string)
    if isinstance(dict_json, dict):
        return dict_json
    else:
        raise ValueError()

def main(pythonpath, pyentry, pyreqs, pyargs, pykwargs, codeurl):

    LOG.info("Batch entrypoint START")
    LOG.info("------------------------------")
    LOG.info(f"pythonpath: {pythonpath}")
    LOG.info(f"pyentry: {pyentry}")
    LOG.info(f"pyreqs: {pyreqs}")
    LOG.info(f"pyargs: {pyargs}")
    LOG.info(f"pykwargs: {pykwargs}")
    LOG.info(f"codeurl: {codeurl}")
    LOG.info("------------------------------")

    # Create working dir
    temp_dir = tempfile.mkdtemp()
    os.chdir(temp_dir)
    sys.path.append(temp_dir)

    LOG.info(f"Working dir is: {os.getcwd()}")

    # localize code from S3
    cp_cmd = f"aws s3 cp --recursive {codeurl} ."
    subprocess.check_call(cp_cmd, shell=True)

    if pyreqs:
        if not os.path.isfile(pyreqs):
            LOG.error(f"Specified reqs file {pyreqs} is not valid: "
                        f"exists: {os.path.exists(pyreqs)}, "
                        f"isfile: {os.path.isfile(pyreqs)}"
                        )
            sys.exit(1)
        cmd=f"conda env update -q -n base -f {pyreqs}"
        subprocess.check_call(cmd, shell=True)

    if pythonpath:
        sys.path.extend([os.path.realpath(p) for p in pythonpath])

    LOG.info(f"sys.path is {sys.path}")

    (entry_module_name, entry_func_name) = pyentry.split(':')
    entry_module = importlib.import_module(entry_module_name)
    entry_func = getattr(entry_module, entry_func_name)

    pyargs = pyargs or []
    pykwargs = pykwargs or {}
    rc = entry_func(*pyargs, **pykwargs)
    LOG.info(f"Batch entrypoint exit: {rc}")
    return rc

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Docker entrypoint for AWS Batch ML.",
        epilog="NOTE: if option takes value, option may be in the form --opt=value or --opt value",
    )

    parser.add_argument(
        "pyentry",
        help="Python entrypoint in the form 'module:function'",
    )

    parser.add_argument(
        "--pythonpath",
        dest="pythonpath",
        nargs="*",
        help="Additional path (relative to base of S3 bucket location) to add to sys.path",
    )

    parser.add_argument(
        "--py-reqsfile",
        dest="pyreqs",
        nargs="?",
        help="The filename (and path if not located at root of url) of a python requirements file in pip or conda format.",
    )
    parser.add_argument(
        "--py-args",
        dest="pyargs",
        type=json_list,
        nargs="?",
        help="A json string of listed positional arguments passed to the python script, e.g. --py-args '[64, \"Another_value\"]'",
    )
    parser.add_argument(
        "--py-kwargs",
        dest="pykwargs",
        type=json_dict,
        nargs="?",
        help='A json string of keyword-value pairs passed as arguments to the python script, e.g. --py-kwargs \'{"KWARG1": 64, "KWARG2": "Another_value"}\'',
    )
    parser.add_argument(
        "--codeurl",
        dest="codeurl",
        nargs="?",
        help="S3 URL to the base of where the code has been copied.",
    )

    args = vars(parser.parse_args())
    sys.exit(main(**args))

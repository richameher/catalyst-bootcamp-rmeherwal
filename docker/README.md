# A Common DockerFile for ML Jobs in Batch and Sagemaker
This dockerfile can be used for both AWS Batch and AWS Sagemaker.
The default entrypoint `sm_train_and_serve.R` provides access to training and serving
R scripts with Sagemaker provided those scripts reflect the pattern in `userFile.R`.
To use the batch commands, you must override the entrypoint with either `batch_git_and_run.py`
passsing it the command line arguments required of the python script to be run. See documentation
in `batch_git_and_run.py` for details.

# Dependencies
Dependency management for python and R are managed by Conda. Runtime dependencies for batch
jobs can be installed. See `batch_git_and_run.py` for details.


# References
https://docs.aws.amazon.com/sagemaker/latest/dg/your-algorithms-training-algo-dockerfile.html
https://docs.aws.amazon.com/sagemaker/latest/dg/your-algorithms-inference-code.html
https://aws.amazon.com/blogs/compute/creating-a-simple-fetch-and-run-aws-batch-job/

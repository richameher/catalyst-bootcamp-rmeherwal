import json
import subprocess


def run_r(*args, **kwargs) -> int:
    cmds = ["Rscript", "/exec-shim/shim.R", json.dumps(args), json.dumps(kwargs)]
    res = subprocess.run(cmds)
    return res.returncode

def run_shell(*args, **kwargs) -> int:
    shim_user_file = kwargs['shim_user_file']
    del kwargs['shim_user_file']
    cmds = ["/bin/bash", shim_user_file]
    cmds.extend(args)
    res = subprocess.run(cmds)
    return res.returncode
